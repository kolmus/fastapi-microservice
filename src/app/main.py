from fastapi import FastAPI
from datetime import datetime
import socket
import uvicorn

app = FastAPI()


@app.get("/")
def read_root():
    now = datetime.now()
    dt_str = now.strftime("%Y.%m.%d %H:%M")
    return {
        "ip": socket.gethostbyname(socket.gethostname()),
        "date": dt_str
        }


if __name__ == '__main__':
    uvicorn.run(app, port=8080, host="0.0.0.0")
