from fastapi.testclient import TestClient
import re

from main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", response.json()['ip'])
    assert re.match(r"\d{4}.\d{2}.\d{2} \d{2}:\d{2}", response.json()['date'])
